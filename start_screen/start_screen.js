const button = document.querySelector('.sidebar-toggle');
const wrapper = document.querySelector('.wrapper');

button.onclick = function (e) {
  e.preventDefault();
  wrapper.classList.toggle('sidebar-collapse');
};

setTimeout(function () {
    let x = window.matchMedia("(min-width: 1264px)");
    if (x.matches) {
        wrapper.classList.toggle('sidebar-collapse');
    }
    wrapper.classList.add('success');
}, 2500);






