const button = document.querySelector('.btn');
const login = document.querySelector('.login');

button.addEventListener('click', function (e) {
    e.preventDefault();
    login.classList.add('content-loading');
    setTimeout(function () {
        login.classList.remove('content-loading');
        login.classList.toggle('content-success');
    }, 5000)
});